<?php

namespace Tests;

use App\Compilation;
use App\User;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @return array
     */
    public function registerUser(string $name, string $email, string $password)
    {
        $response = $this->post('/register', [
            'name'                  => $name,
            'email'                 => $email,
            'password'              => $password,
            'password_confirmation' => $password,
        ]);
        $user = User::where('email', $email)->first();
        return [$response, $user];
    }

    /**
     * @param $videoId
     * @param $title
     * @return array
     */
    public function createCompilation($videoId, $title): array
    {
        $response = $this->post('/compilations', [
            'youtube_url' => "https://www.youtube.com/watch?v={$videoId}",
            'title'       => $title,
            'description' => 'dummy description',
            'source'      => 'text',
            'playlist'    => "00:00 - Bobby Darin - Dream Lover\r\n02:31 - Bobby Darin - Splish Splash\r\n04:45 - Bobby Darin - Mack The Knife\r\n07:57	- Bobby Darin - Beyond The Sea\r\n"
        ]);

        $compilation = Compilation::where('title', $title)->first();
        return [$response, $compilation];
    }
}
