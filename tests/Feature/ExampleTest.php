<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testStoreCompilation()
    {
        $email = 'jernej@example.si';
        $videoId = 'oqsv-kYrD-M';
        $title = 'Bobby Darin - Best 50 songs - Music Legends Book';

        $user = $this->registerUser('jernej', $email, 'jernej')[1];
        list($response, $compilation) = $this->createCompilation($videoId, $title);

        $this->assertDatabaseHas('compilations', [
            'title'    => $title,
            'video_id' => $videoId,
            'user_id'  => $user->id
        ]);

        $this->assertDatabaseHas('items', [
            'artist'         => 'Bobby Darin',
            'title'          => 'Dream Lover',
            'compilation_id' => $compilation->id,
            'starts_at_text' => '00:00',
            'starts_at_secs' => 0
        ]);

        $this->assertDatabaseHas('items', [
            'artist'         => 'Bobby Darin',
            'title'          => 'Splish Splash',
            'compilation_id' => $compilation->id,
            'starts_at_text' => '02:31',
            'starts_at_secs' => 151
        ]);

        $this->assertDatabaseHas('items', [
            'artist'         => 'Bobby Darin',
            'title'          => 'Mack The Knife',
            'compilation_id' => $compilation->id,
            'starts_at_text' => '04:45',
            'starts_at_secs' => 285
        ]);

        $this->assertDatabaseHas('items', [
            'artist'         => 'Bobby Darin',
            'title'          => 'Beyond The Sea',
            'compilation_id' => $compilation->id,
            'starts_at_text' => '07:57',
            'starts_at_secs' => 477
        ]);

        $response->assertRedirect(route('compilations.show', [
            'id' => $compilation->id
        ]));
    }

    function testStoreVoteFirstTime() {
        $videoId = 'oqsv-kYrD-M';
        $title = 'Bobby Darin - Best 50 songs - Music Legends Book';

        $user = $this->registerUser('jernej', 'jernej.sila@gmail.com', 'jernej')[1];
        $compilation = $this->createCompilation($videoId, $title)[1];

        $this->post(route('compilations.storeVote', $compilation), ['vote' => '1']);

        $this->assertDatabaseHas('compilation_likes', [
            'compilation_id' => $compilation->id,
            'user_id'        => $user->id,
            'vote'           => 1
        ]);
    }

    function testStoreVoteChangeVote() {
        $videoId = 'oqsv-kYrD-M';
        $title = 'Bobby Darin - Best 50 songs - Music Legends Book';

        $user = $this->registerUser('jernej', 'jernej.sila@gmail.com', 'jernej')[1];
        $compilation = $this->createCompilation($videoId, $title)[1];

        $this->post(route('compilations.storeVote', $compilation), ['vote' => '1']);
        $this->post(route('compilations.storeVote', $compilation), ['vote' => '0']);

        $this->assertDatabaseHas('compilation_likes', [
            'compilation_id' => $compilation->id,
            'user_id'        => $user->id,
            'vote'           => 0
        ]);
    }

    function testStoreVoteDeleteVote() {
        $videoId = 'oqsv-kYrD-M';
        $title = 'Bobby Darin - Best 50 songs - Music Legends Book';

        $user = $this->registerUser('jernej', 'jernej.sila@gmail.com', 'jernej')[1];
        $compilation = $this->createCompilation($videoId, $title)[1];

        $this->post(route('compilations.storeVote', $compilation), ['vote' => '1']);
        $this->post(route('compilations.storeVote', $compilation), ['vote' => '1']);

        $this->assertDatabaseMissing('compilation_likes', [
            'compilation_id' => $compilation->id,
            'user_id'        => $user->id,
        ]);
    }
}
