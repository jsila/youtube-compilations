<?php

function secsToText($secs) {
    $h = intdiv($secs, 3600);
    $m = intdiv($secs - $h * 3600, 60);
    $s = $secs - ($h * 3600) - ($m * 60);
    return sprintf('%02d', $h) . ":" . sprintf('%02d', $m) . ":" . sprintf('%02d', $s);
}

function textToSecs($text) {
    $parts = explode(':', $text);
    if (count($parts) == 2) {
        list($m, $s) = $parts;
        $h = 0;
    } else {
        list($h, $m, $s) = $parts;
    }
    return $h*3600 + $m*60 + $s;
}

function parse_url_and_query($url) {
    $url = parse_url($url);
    parse_str($url['query'], $query);
    $url['query'] = $query;
    return $url;
}

function chooseClass($cond, $ifTrue, $ifFalse) {
    return $cond == true ? $ifTrue : $ifFalse;
}