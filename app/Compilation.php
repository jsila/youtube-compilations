<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Compilation extends Model
{
    protected $fillable = ['title', 'description', 'video_id'];

    static function determineSource(Request $request) {
        $source = $request->query('source');
        switch ($source) {
            case 'text': break;
            default:
                $source = 'file';
        }
        return $source;

    }

    public function voters() {
        return $this->belongsToMany(User::class, 'compilation_likes')->withTimestamps();
    }

    public function items() {
        return $this->hasMany(Item::class);
    }

    public function creator() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function settings() {
        return $this->hasMany(CompilationSettings::class);
    }
}
