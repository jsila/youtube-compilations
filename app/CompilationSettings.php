<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompilationSettings extends Model
{
    const REPEAT = 1;
    const SHUFFLE = 2;

    const all = [
        self::REPEAT,
        self::SHUFFLE
    ];

    protected $fillable = ['id', 'user_id', 'compilation_id', 'value'];

    public function compilation() {
        $this->belongsTo(Compilation::class);
    }

    public function user() {
        $this->belongsTo(User::class);
    }

}
