<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompilationLike extends Model
{
    protected $fillable = [
        'compilation_id',
        'user_id',
        'vote'
    ];
}
