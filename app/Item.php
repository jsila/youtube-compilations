<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['artist', 'title', 'starts_at_text', 'starts_at_secs'];

    public function selectedByUsers() {
        return $this->belongsToMany(User::class, 'user_selected_items')
            ->withTimestamps();
    }

    public function compilation() {
        return $this->belongsTo(Compilation::class);
    }
}
