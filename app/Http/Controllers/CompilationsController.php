<?php

namespace App\Http\Controllers;

use App\Compilation;
use App\CompilationLike;

use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class CompilationsController extends Controller
{
    /**
     * @var Compilation
     */
    protected $compilation;

    /**
     * @var CompilationLike
     */
    protected $compilationLike;

    public function __construct(Compilation $compilation, CompilationLike $compilationLike)
    {
        $this->compilation         = $compilation;
        $this->compilationLike     = $compilationLike;

        $this->middleware('auth')->except('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $compilations = $this->compilation->all();

        return view('compilations.index', compact('compilations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $source = Compilation::determineSource($request);

        return view('compilations.create', compact('source'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $compilation = $request->user()->compilations()->create([
            'title'       => $request->get('title'),
            'description' => $request->get('description'),
            'video_id'    => parse_url_and_query($request->get('youtube_url'))['query']['v']
        ]);

        $playlist = $request->get('source') === 'text' ? $request->get('playlist') : file_get_contents($request->file('playlist')->getRealPath());
        $playlist = explode(PHP_EOL, trim($playlist));

        foreach ($playlist as $song) {
            list($time, $artist, $title) = explode('-', $song);
            $compilation->items()->create([
                'artist'         => trim($artist),
                'title'          => trim($title),
                'starts_at_text' => trim($time),
                'starts_at_secs' => textToSecs(trim($time))
            ]);
        }

        return redirect()->route('compilations.show', ['id' => $compilation->id])
            ->with('status', 'New compilation successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param Compilation $compilation
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(Compilation $compilation, Request $request)
    {
        $likes = $this->compilationLike->where([
            'vote'           => true,
            'compilation_id' => $compilation->id
        ])->count();

        $dislikes = $this->compilationLike->where([
            'vote'           => false,
            'compilation_id' => $compilation->id
        ])->count();

        $settings = $compilation->settings()
            ->where('user_id', $request->user()->id)
            ->pluck('value', 'id');

        try {
            $vote = (bool) $this->compilationLike->where([
                'compilation_id' => $compilation->id,
                'user_id'        => $request->user()->id
            ])->firstOrFail()->vote;
        } catch (\Exception $e) {
            $vote = null;
        }
        return view('compilations.show', compact('compilation', 'likes', 'dislikes', 'vote', 'settings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Compilation  $compilation
     * @return \Illuminate\Http\Response
     */
    public function edit(Compilation $compilation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Compilation  $compilation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Compilation $compilation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Compilation  $compilation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Compilation $compilation)
    {
        //
    }

    public function storeVote(int $compilationId, Request $request)
    {
        $vote   = $request->post('vote');
        $userId = $request->user()->id;

        $data = [
            'compilation_id' => $compilationId,
            'user_id'        => $userId,
            'vote'           => $vote
        ];

        $deletedRows = $this->compilationLike->where($data)->delete();

        if ($deletedRows != 0) {
            return back()->with('status', 'Vote removed');
        }

        $compilationLike = $this->compilationLike->updateOrCreate([
            'compilation_id' => $compilationId,
            'user_id'        => $userId
        ], [
            'vote'           => $vote
        ]);

        $status = $compilationLike->wasRecentlyCreated ? 'Vote saved' : 'Vote changed';

        return back()->with('status', $status);
    }

    public function storeSettings(Compilation $compilation, Request $request)
    {
        $compilation->settings()->updateOrCreate([
            'id'      => $request->post('id'),
            'user_id' => $request->user()->id
        ], [
            'value'   => $request->post('value'),
        ]);

        return response(null, SymfonyResponse::HTTP_OK);
    }

    public function showUserCompilations(int $userId)
    {
        $compilations = $this->compilation->where('user_id', $userId)->get();

        return view('users.compilations', compact('compilations'));
    }
}
