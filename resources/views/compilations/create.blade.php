@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create new compilation</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form class="form-horizontal" action="{{ route('compilations.store') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="youtube_url" class="col-md-3">YouTube URL</label>
                                <div class="col-md-9">
                                    <input type="url" class="form-control" id="youtube_url" name="youtube_url">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title" class="col-md-3">Title</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="title" name="title">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description" class="col-md-3">Description/Comment</label>
                                <div class="col-md-9">
                                    <textarea name="description" id="description" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <label for="playlist">Playlist</label>
                                    <p class="help-block small">Preferred format<br/>[time] - [artist] - [song]</p>
                                </div>

                                <div class="col-md-9">

                                    <div>
                                        <div class="btn-group" role="group" aria-label="Playlist source" style="margin-bottom: 1em;">
                                            <a href="{{ route('compilations.create') }}" class="btn btn-sm {{ $source == "file" ? "btn-info" : "btn-default" }}">File</a>
                                            <a href="{{ route('compilations.create', ['source' => 'text'])  }}" class="btn btn-sm {{ $source == "text" ? "btn-info" : "btn-default" }}">Text</a>
                                        </div>
                                    </div>

                                    <input type="hidden" name="source" id="source" value="{{ $source }}">

                                    @if($source == "text")
                                    <textarea name="playlist" id="playlist" class="form-control"></textarea>
                                    @else
                                    <input type="file" name="playlist" id="playlist">
                                    @endif
                                </div>
                            </div>

                            <div class="form-group" style="margin-top: 2em;">
                                <div class="col-md-9 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
