@extends('layouts.app')

@section('header')
    <meta property="og:image" content="https://img.youtube.com/vi/{{ $compilation->id }}/maxresdefault.jpg">
    <meta property="og:title" content="{{ $compilation->title }}">
    @if ($compilation->description)
    <meta property="og:description" content="{{ $compilation->description }}">
    @endif
    <meta property="og:url" content="{{ request()->url() }}">
    <meta name="twitter:card" content="summary_large_image">

    <style>
        .videowrapper {
            position: relative;
            padding-bottom: 56.25%; /* 16:9 */
            padding-top: 25px;
            height: 0;
        }
        .videowrapper iframe {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
        #playlist {
            margin-top: 1em;
        }
        .song-title {
            cursor: pointer;
        }
        .success .song-title {
            font-weight: bold;
        }
        form {
            display: inline;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ $compilation->title }}
                        <small class="pull-right text-muted" style="margin-top: 2px;">added by <a href="{{ route('users.show', $compilation->creator) }}">{{ $compilation->creator->name }}</a></small>
                    </div>

                    <div class="panel-body">
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif

                        @if($compilation->description)
                        <p class="lead">{{ $compilation->description }}</p>
                        <hr>
                        @endif

                        <div class="videowrapper">
                            <iframe id="player" width="560" height="315" src="https://www.youtube.com/embed/{{ $compilation->video_id }}?enablejsapi=1" frameborder="0" allowfullscreen></iframe>
                        </div>

                        <div class="clearfix" style="margin-top: 1em;">
                            <div class="btn-group btn-group-sm pull-left">
                                <button id="shuffle" title="Shuffle" data-value="{{ $settings->get(\App\CompilationSettings::SHUFFLE) }}" class="btn {{ chooseClass($settings->get(\App\CompilationSettings::SHUFFLE), 'btn-info', 'btn-default') }}"><i class="fa fa-random"></i></button>
                                <button id="repeat" title="Repeat" data-value="{{ $settings->get(\App\CompilationSettings::REPEAT) }}" class="btn {{ chooseClass($settings->get(\App\CompilationSettings::REPEAT), 'btn-info', 'btn-default') }}"><i class="fa fa-repeat"></i></button>
                            </div>
                            <div class="pull-right">
                                <div class="btn-group btn-group-sm">
                                    <button title="Like" class="btn {{ chooseClass($vote === true, 'btn-info', 'btn-default') }}">{{ $likes }} <i class="fa fa-thumbs-up"></i></button>
                                    <button title="Dislike" class="btn {{ chooseClass($vote === false, 'btn-info', 'btn-default') }}">{{ $dislikes }} <i class="fa fa-thumbs-down"></i></button>
                                </div>
                                <button title="Share" class="btn btn-sm btn-default"><i class="fa fa-share-alt"></i></button>
                            </div>
                        </div>

                        <hr>

                        <h3 class="text-center">Playlist</h3>

                        <table class="table table-striped" id="playlist">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Starts At</th>
                                <th>Song</th>
                                <th>
                                    <input id="selectall" type="checkbox" title="Select all" />
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="4">
                                    <p class="text-center text-muted"><em>Please wait while fetching playlist...</em></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script defer src="https://www.youtube.com/iframe_api"></script>
    <script defer src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script defer src="https://cdn.jsdelivr.net/npm/lodash@4.17.4/lodash.min.js"></script>
    <script defer src="{{ asset('js/compilation.js') }}"></script>
@endsection