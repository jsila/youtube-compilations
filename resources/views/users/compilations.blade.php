@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Compilations</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        @forelse ($compilations->chunk(2) as $compilationChunk)
                            <div class="row">
                                @foreach($compilationChunk as $compilation)
                                    <div class="col-xs-6">
                                        <div class="thumbnail">
                                            <img src="https://img.youtube.com/vi/{{ $compilation->video_id }}/mqdefault.jpg" alt="Thumbnail of {{ $compilation->title }}">
                                            <div class="caption">
                                                <h4><a href="/compilations/{{$compilation->id}}">{{ $compilation->title }}</a></h4>
                                                <p>{{ $compilation->description }}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @empty
                            <p>User hasn't submitted any compilation yet.</p>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
