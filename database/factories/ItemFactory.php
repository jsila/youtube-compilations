<?php

use App\Item;
use App\Compilation;
use Faker\Generator as Faker;

$factory->define(Item::class, function (Faker $faker) {
    $n = $faker->numberBetween(0, 60*60*3);
    return [
        'artist'         => $faker->firstName . ' ' . $faker->lastName,
        'title'          => ucfirst(implode(' ', $faker->words(5))),
        'compilation_id' => factory(Compilation::class)->create()->id,
        'starts_at_secs' => $n,
        'starts_at_text' => secsToText($n)
    ];
});
