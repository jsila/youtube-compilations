<?php

use App\Compilation;
use App\User;
use Faker\Generator as Faker;

$factory->define(Compilation::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(User::class)->create()->id;
        },
        'title'   => ucfirst(implode(' ', $faker->words(6)))
    ];
});
