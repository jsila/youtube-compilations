<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/compilations', 'CompilationsController');

Route::post('/compilations/{compilation}/votes', 'CompilationsController@storeVote')
    ->name('compilations.storeVote');

Route::put('/compilations/{compilation}/settings', 'CompilationsController@storeSettings')
    ->name('compilations.storeSettings');

Route::resource('/users', 'UsersController');
Route::get('/users/{user}/compilations', 'CompilationsController@showUserCompilations')
    ->name('compilations.showUserCompilations');
