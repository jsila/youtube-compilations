<?php

use App\Compilation;
use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/compilations/{compilation}/items', function (Compilation $compilation) {
    return $compilation->items()->get();
});