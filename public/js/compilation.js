(function() {
    const select = sel => document.querySelector(sel)
    const selectAll = sel => document.querySelectorAll(sel)
    const setHTML = (sel, html) => select(sel).innerHTML = html
    const on = (event, sel, cb) => selectAll(sel).forEach(el => el.addEventListener(event, cb, false))
    const closest = (sel, el) => el.matches(sel) ? el : closest(sel, el.parentNode)
    const data = (attr, sel) => select(sel).dataset[attr]

    function shuffle(a) {
        const b = [...a]
        let j, x, i;
        for (i = b.length; i; i--) {
            j = Math.floor(Math.random() * i);
            x = b[i - 1];
            b[i - 1] = b[j];
            b[j] = x;
        }
        return b
    }

    function renderSong({id, artist, title, starts_at_secs, starts_at_text, selected = false, active = false}) {
        const checked = selected ? 'checked' : ''
        const songClassnames = active ? 'class="success"' : ''

        return `<tr ${songClassnames} data-starts-at="${starts_at_secs}" data-id="${id}">
    <th>${id}</th>
    <td>${starts_at_text}</td>
    <td class="song-title">${artist} - ${title}</td>
    <td>
        <input class="select" type="checkbox" ${checked}>
    </td>
</tr>`
    }

    let player
    let checking
    let songs = []
    let songIds = []
    let currentActiveSong = 0
    let allSongsSelected = true
    let selectedSongs = []
    let settings = {
        shuffle: {
            id: 2,
            value: !!+data('value', 'button#shuffle')
        },
        repeat: {
            id: 1,
            value: !!+data('value', 'button#repeat')
        }
    }

    function setCurrentActiveSongCB(event) {
        songs = setCurrentActiveSong(event.target.parentNode.dataset, songs)
        renderSongs(songs)
    }

    function setSongSelectedCB(event) {
        const el = event.target
        songs = setSongSelected(el.checked, el.parentNode.parentNode.dataset.id, songs)
        selectedSongs = songs.filter(s => s.selected).map(s => s.id)
        if (selectedSongs.length === 0 || selectedSongs === songIds.length) {
            selectedSongs = [...songIds]
            allSongsSelected = true
        } else {
            allSongsSelected = false
        }
        renderSongs(songs)
    }

    function selectAllCB(event) {
        songs = [...songs].map(s => Object.assign({}, s, {selected: event.target.checked}))
        allSongsSelected = true
        selectedSongs = [...songIds]
        renderSongs(songs)
    }

    function setCurrentActiveSong(data, songs) {
        let newSongs = [...songs]
        if (currentActiveSong) {
            newSongs = _.update(newSongs, currentActiveSong-1, s =>
                Object.assign({}, s, {active: false})
            )
        }

        currentActiveSong = +data.id

        if (typeof player !== 'undefined') {
            player.seekTo(+data.startsAt, true).playVideo()
        }

        return _.update(newSongs, data.id-1, s =>
            Object.assign({}, s, {active: true})
        )
    }

    function setSongSelected(selected, id, songs) {
        return _.update([...songs], [id-1], s =>
            Object.assign({}, s, {selected})
        )
    }

    function repeatCB(event) {
        event.preventDefault()
        const el = closest('button', event.target)

        axios.put(`${window.location.pathname}/settings`, {id: settings.repeat.id, value: !settings.repeat.value})
            .then(res => {
                settings.repeat.value = !settings.repeat.value
                el.classList.toggle('btn-default')
                el.classList.toggle('btn-info')
            })
    }

    function shuffleCB(event) {
        event.preventDefault()
        const el = closest('button', event.target)

        axios.put(`${window.location.pathname}/settings`, {id: settings.shuffle.id, value: !settings.shuffle.value})
            .then(res => {
                settings.shuffle.value = !settings.shuffle.value
                el.classList.toggle('btn-default')
                el.classList.toggle('btn-info')

                if (settings.shuffle.value) {
                    selectedSongs = [currentActiveSong, ...shuffle(selectedSongs.filter(n => n !== currentActiveSong))]
                } else {
                    selectedSongs.sort()
                }
                console.log(selectedSongs)
            })
    }

    function renderSongs(songs) {
        const target = '#playlist tbody'

        setHTML(target, songs.map(renderSong).join(''))

        on('click', target + ' .song-title', setCurrentActiveSongCB)
        on('change', target + ' .select', setSongSelectedCB)
    }

    on('change', '#selectall', selectAllCB)
    on('click', '#repeat', repeatCB)
    on('click', '#shuffle', shuffleCB)

    axios.get(`/api${window.location.pathname}/items`).then(res => {
        songs = res.data
        songIds = res.data.map(s => s.id)
        selectedSongs = [...songIds]

        if (settings.shuffle.value) {
            selectedSongs = shuffle(songIds)
        }

        renderSongs(songs)

        YT.ready(() => {
            player = new YT.Player('player', {
                events: {
                    onReady,
                    onStateChange
                }
            })
        })
    })

    function onReady() {
        const usp = new URLSearchParams(window.location.search)
        const playSongId = +usp.get('song')
        const dontPlay = usp.has('dontPlay')

        if (!!playSongId && playSongId > 0 && playSongId <= songs.length) {
            const {startsAt} = songs[playSongId-1].dataset
            player.seekTo(+startsAt, true)
        }

        if(!dontPlay) {
            return player.playVideo()
        }
    }

    function stopPlaying() {
        player.stopVideo()
        clearInterval(checking)

        songs = _.update(songs, currentActiveSong-1, s =>
            Object.assign({}, s, {active: false})
        )

        renderSongs(songs)
    }

    function setCurrentSong() {
        const currentTime = player.getCurrentTime()

        let id = songs.length - 1
        for (let i = 0; i < songs.length-1; i++) {
            const lo = songs[i].starts_at_secs
            const hi = songs[i+1].starts_at_secs
            if (currentTime >= lo && currentTime < hi) {
                id = i
                break
            }
        }

        if (currentActiveSong === id + 1) return

        const selectedSongId = selectedSongs.findIndex(id2 => id2 === currentActiveSong) || 0

        let id2 = selectedSongs[selectedSongId+1]

        if (id2 === undefined) { // we came to the end of selected songs
            if (settings.repeat.value) {
                id2 = selectedSongId[0]
            } else {
                stopPlaying()
                return
            }
        }

        songs = setCurrentActiveSong({
            id: id2,
            startsAt: songs[id2-1].starts_at_secs
        }, songs)

        renderSongs(songs)
    }

    function onStateChange(event) {
        switch (event.data) {
            case YT.PlayerState.PLAYING:
                setCurrentSong()
                checking = setInterval(setCurrentSong, 1000)
                break
            default:
                clearInterval(checking)
                break
        }
    }
})()
